package txgroup.trialapp.alexruskovski.data.local

import android.app.Application
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AppDataBaseTest {

    private lateinit var bookmarksDao: BookmarksDao
    private lateinit var appDatabase: AppDataBase

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val dispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(dispatcher)

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Application>()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDataBase::class.java
        ).build()
        bookmarksDao = appDatabase.bookmarksDao()
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testBookmarkingSiteSucceed(){
        runBlocking {
            bookmarksDao.insert(fakeBookmark)
            val bookmarks = bookmarksDao.getAll()
            assertThat(bookmarks).contains(fakeBookmark)
        }
    }


    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testRemovingBookmark(){
        runBlocking {
            bookmarksDao.insert(fakeBookmark)
            val bookmarks = bookmarksDao.getAll()
            assertThat(bookmarks).contains(fakeBookmark)
            bookmarksDao.delete(fakeBookmark)
            assertThat(bookmarksDao.getAll()).isEmpty()
        }
    }

    /** HELPER METHODS **/
    private val fakeBookmark = BookmarkEntity(
        "www.testing.com", "test title"
    )


}