package txgroup.trialapp.alexruskovski.presentation

import android.app.Activity
import android.view.LayoutInflater
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import txgroup.trialapp.alexruskovski.R


/**
 * Created by Alexander Ruskovski on 08/03/2022
 * Contains Fragment general extensions
 */


private var alertDialog: AlertDialog ?= null

fun Fragment.showDialog(
    title: String,
    message: String,
    isCancelable: Boolean = false,
    positiveText: String = "OK",
    negativeText: String ?= null,
    onPositive: () -> Unit = {},
    onNegative: () -> Unit = {}
){
    if(alertDialog?.isShowing == true)
        return

    val b = AlertDialog.Builder(requireContext())
    b.setTitle(title)
    b.setMessage(message)
    b.setCancelable(isCancelable)
    b.setIcon(R.drawable.favicon)
    b.setPositiveButton(positiveText){d, _ ->
        d.dismiss()
        onPositive.invoke()
    }

    if(negativeText != null){
        b.setNegativeButton(negativeText){d, _ ->
            d.dismiss()
            onNegative.invoke()
        }
    }

    alertDialog = b.create()

    alertDialog?.window?.setBackgroundDrawableResource(R.drawable.dialog_rounded_corners)
    alertDialog?.window?.attributes?.windowAnimations = R.style.AlertDialogSlideUpSlideDown
    alertDialog?.show()
}


fun Fragment.showBookmarkDialog(
    url: String,
    onSaveBookmark: (String) -> Unit
){

    alertDialog?.dismiss()

    val b = AlertDialog.Builder(requireContext())
    b.setIcon(R.drawable.favicon)

    val view = LayoutInflater.from(requireContext())
        .inflate(R.layout.dialog_with_text_input, null, false)
    val etTitle = view.findViewById<EditText>(R.id.etBookmarkTitle)
    val etUrl = view.findViewById<EditText>(R.id.etUrl)
    etUrl.isEnabled = false
    etUrl.setText(url)
    b.setView(view)
    b.setPositiveButton(getString(R.string.save_bookmark_dialog_positive_btn), null)
    b.setNegativeButton(getString(R.string.save_bookmark_dialog_negative_btn)){d, _ ->
        d.dismiss()
    }
    alertDialog = b.create()

    alertDialog?.window?.setBackgroundDrawableResource(R.drawable.dialog_rounded_corners)
    alertDialog?.window?.attributes?.windowAnimations = R.style.AlertDialogSlideUpSlideDown

    alertDialog?.setOnShowListener {
        val positiveBtn = alertDialog?.getButton(AlertDialog.BUTTON_POSITIVE)
        positiveBtn?.setOnClickListener {
            val bookmarkTitle = etTitle.text.toString()
            if(bookmarkTitle.trim().isEmpty()){
                etTitle.error = getString(R.string.save_bookmark_dialog_error)
                return@setOnClickListener
            }
            onSaveBookmark.invoke(bookmarkTitle)
            alertDialog?.dismiss()
        }
    }

    alertDialog?.show()

}


