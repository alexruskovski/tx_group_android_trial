package txgroup.trialapp.alexruskovski.presentation.fragment_webview

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import txgroup.trialapp.alexruskovski.data.local.BookmarkEntity
import txgroup.trialapp.alexruskovski.data.local.BookmarksDao
import txgroup.trialapp.alexruskovski.data.preferences.AppPrefs
import javax.inject.Inject

/**
 * Created by Alexander Ruskovski on 07/03/2022
 */

@HiltViewModel
class WebViewFragmentViewModel @Inject constructor(
    private val bookmarksDao: BookmarksDao,
    private val appPrefs: AppPrefs
): ViewModel() {

    var showInitialLoading: Boolean = true
    var isCurrentPageBookmarked = false
    private val _isCurrentPageBookmarked = MutableStateFlow(false)

    fun isCurrentPageBookmarked():StateFlow<Boolean> = _isCurrentPageBookmarked

    fun onSaveBookmark(url: String, bookmarkTitle: String){
        viewModelScope.launch(Dispatchers.IO){
            bookmarksDao.insert(BookmarkEntity(url, bookmarkTitle))
            _isCurrentPageBookmarked.value = true
        }
    }

    fun onPageLoaded(url: String){
        viewModelScope.launch {
            val bookmarks = bookmarksDao.getAll()
            _isCurrentPageBookmarked.value = bookmarks.contains(BookmarkEntity(url, ""))
        }
    }

    fun removeBookmark(url: String){
        viewModelScope.launch {
            bookmarksDao.delete(BookmarkEntity(url, ""))
            _isCurrentPageBookmarked.value = false
        }
    }

    fun isBookmarkedDialogEnabled(): Boolean{
        return appPrefs.isBookmarkedDialogEnabled()
    }

    fun setIsBookmarkedDialogEnabled(isEnabled: Boolean){
        appPrefs.setIsBookmarkedDialogEnabled(isEnabled)
    }


    fun getAllBookmarks() = flow {
        val bookmarks = bookmarksDao.getAll()
        emit(bookmarks)
    }

}