package txgroup.trialapp.alexruskovski.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import txgroup.trialapp.alexruskovski.R

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun setNavigateUp(isEnabled: Boolean){
        supportActionBar?.setDisplayHomeAsUpEnabled(isEnabled)
    }

    fun setToolbarTitle(title: String){
        supportActionBar?.title = title
    }
}