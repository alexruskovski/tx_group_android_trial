package txgroup.trialapp.alexruskovski.presentation.fragment_webview.bookmarks

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import txgroup.trialapp.alexruskovski.data.local.BookmarkEntity
import txgroup.trialapp.alexruskovski.databinding.BookmarksAdapterItemBinding


/**
 * Created by Alexander Ruskovski on 09/03/2022
 */

class BookmarksAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val data = mutableListOf<BookmarkEntity>()
    private var onDeleteBookmark: (BookmarkEntity, Int) -> Unit = {_, _->}
    private var onBookmarkClicked: (BookmarkEntity) -> Unit = { }

    fun init(
        data: List<BookmarkEntity>,
        onDeleteBookmark: (BookmarkEntity, Int) -> Unit,
        onBookmarkClicked: (BookmarkEntity) -> Unit
    ){
        this.onDeleteBookmark = onDeleteBookmark
        this.onBookmarkClicked = onBookmarkClicked
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return VH(BookmarksAdapterItemBinding.inflate(inflater, parent, false), onDeleteBookmark, onBookmarkClicked)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VH).bind(data[position], position)
    }

    override fun getItemCount() = data.size

    inner class VH(
        private val binding: BookmarksAdapterItemBinding,
        private val onDeleteBookmark: (BookmarkEntity, Int) -> Unit,
        private val onBookmarkClicked: (BookmarkEntity) -> Unit
    ): RecyclerView.ViewHolder(binding.root){


        fun bind(bookmarkEntity: BookmarkEntity, index: Int){

            binding.root.setOnClickListener {
                onBookmarkClicked.invoke(bookmarkEntity)
            }

            binding.tvBookmarkName.text = bookmarkEntity.title
            binding.imageView2.setOnClickListener {
                try {
                    data.removeAt(index)
                } catch (ex: java.lang.IndexOutOfBoundsException){
                    //swallow the exception
                }
                onDeleteBookmark.invoke(bookmarkEntity, index)
            }
        }

    }
}