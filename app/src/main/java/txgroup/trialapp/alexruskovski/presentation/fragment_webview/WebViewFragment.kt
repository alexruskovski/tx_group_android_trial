package txgroup.trialapp.alexruskovski.presentation.fragment_webview

import android.animation.ValueAnimator
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import txgroup.trialapp.alexruskovski.R
import txgroup.trialapp.alexruskovski.common.NewsWebView
import txgroup.trialapp.alexruskovski.databinding.FragmentWebviewBinding
import txgroup.trialapp.alexruskovski.presentation.MainActivity
import txgroup.trialapp.alexruskovski.presentation.fragment_webview.bookmarks.BookmarksFragmentDialog
import txgroup.trialapp.alexruskovski.presentation.showBookmarkDialog
import txgroup.trialapp.alexruskovski.presentation.showDialog


/**
 * Created by Alexander Ruskovski on 07/03/2022
 */

@AndroidEntryPoint
class WebViewFragment : Fragment(R.layout.fragment_webview){

    private lateinit var viewBinding: FragmentWebviewBinding

    private lateinit var viewModel: WebViewFragmentViewModel

    private var valueAnimator: ValueAnimator ?= null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding = FragmentWebviewBinding.bind(view)
        viewModel = ViewModelProvider(this).get(WebViewFragmentViewModel::class.java)
        setHasOptionsMenu(true)
        observerWebViewState()
        observerBookmarkState()
        viewBinding.webView.loadHomePage()
        invalidateSwipeToRefreshLayout()
    }

    private fun invalidateSwipeToRefreshLayout(){
        viewBinding.swipeToRefresh.isEnabled = false
        viewBinding.swipeToRefresh.setOnRefreshListener {
            viewBinding.webView.reload()
        }
    }

     private fun rotateLogo(){
        valueAnimator = ValueAnimator.ofFloat(0f, 360f).apply {
            interpolator = LinearInterpolator()
            duration = 999
            repeatCount = ValueAnimator.INFINITE
            addUpdateListener {
                val value = it.animatedValue as Float
                viewBinding.ivTxLogo.rotationX = value
            }
        }
        valueAnimator?.start()
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val bookmarkItem = menu.findItem(R.id.browser_menu_bookmark)
        val removeBookmarkItem = menu.findItem(R.id.browser_menu_remove_bookmark)
        val forwardItem = menu.findItem(R.id.browser_menu_navigate_forward)
        val navBackItem = menu.findItem(R.id.browser_menu_navigate_back)
        bookmarkItem.isVisible = !viewModel.isCurrentPageBookmarked
        bookmarkItem.isEnabled = !viewModel.isCurrentPageBookmarked
        removeBookmarkItem.isVisible = viewModel.isCurrentPageBookmarked
        removeBookmarkItem.isEnabled = viewModel.isCurrentPageBookmarked
        forwardItem.isEnabled = viewBinding.webView.canGoForward()
        navBackItem.isEnabled = viewBinding.webView.canGoBack()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.browser_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.browser_menu_home -> {
                viewBinding.webView.loadHomePage()
                true
            }
            R.id.browser_menu_navigate_back -> {
                viewBinding.webView.navigateBackIfPossible()
                true
            }
            R.id.browser_menu_navigate_forward -> {
                viewBinding.webView.navigateForwardIfCan()
                true
            }
            R.id.browser_menu_bookmark -> {
                onBookmark()
                true
            }
            R.id.browser_menu_remove_bookmark -> {
                onRemoveBookmark()
                true
            }
            R.id.browser_menu_bookmarks -> {
                onViewBookmarks()
                true
            }
            android.R.id.home -> {
                viewBinding.webView.navigateBackIfPossible()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onViewBookmarks(){
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.getAllBookmarks().collectLatest { bookmarks ->
                if(bookmarks.isEmpty()){
                    onNoBookmarks()
                    return@collectLatest
                }
                val dialog = BookmarksFragmentDialog()
                val onLoadFromBookmark: (String) -> Unit = {onLoadFromBookmark(it)}
                dialog.onLoadFromBookmark = onLoadFromBookmark
                val onDismiss: () -> Unit = {
                    //we just want to re-check whether our page is still bookmarked if it was!
                    viewModel.onPageLoaded(viewBinding.webView.url ?: "")
                }
                dialog.onDismiss = onDismiss
                dialog.show(
                    childFragmentManager,
                    BookmarksFragmentDialog::class.simpleName ?: "bookmarksFragmentDialog"
                )
            }
        }
    }

    private fun onLoadFromBookmark(url: String){
        viewBinding.webView.loadUrl(url)
    }

    private fun onNoBookmarks(){
        showDialog(
            title = getString(R.string.no_bookmarks_dialog_title),
            message = getString(R.string.no_bookmarks_dialog_message)
        )
    }


    private fun observerWebViewState(){
        viewLifecycleOwner.lifecycleScope.launch {
            viewBinding.webView.webViewState.collectLatest {
                renderWebViewState(it)
            }
        }
    }

    private fun observerBookmarkState(){
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.isCurrentPageBookmarked().collectLatest {
                viewModel.isCurrentPageBookmarked = it
                requireActivity().invalidateOptionsMenu()
            }
        }
    }

    private fun animateWebViewPage(vararg animValues: Float){
        ValueAnimator.ofFloat(*animValues).apply {
            interpolator = LinearInterpolator()
            duration = 333
            addUpdateListener {
                val value = it.animatedValue as Float
                viewBinding.webView.scaleX = value
                viewBinding.webView.scaleY = value
            }
        }.start()
    }

    private fun onBookmark(){
        showBookmarkDialog(
            viewBinding.webView.url ?: ""
        ){ bookmarkTitle ->
            viewModel.onSaveBookmark(viewBinding.webView.url ?: "", bookmarkTitle)
            if(viewModel.isBookmarkedDialogEnabled()){
                showDialog(
                    title = getString(R.string.bookmarked_title),
                    message = getString(R.string.bookmarked_message, viewBinding.webView.title),
                    negativeText = getString(R.string.bookmarked_negative_text),
                    onNegative = {
                        viewModel.setIsBookmarkedDialogEnabled(false)
                    }
                )
            }
        }
    }

    private fun onRemoveBookmark(){
        animateWebViewPage(1f,1.2f,1f)
        viewBinding.webView.url?.let {
            viewModel.removeBookmark(it)
        }
    }

    private fun setToolbarTitle(newTitle: String){
        if(requireActivity() is MainActivity){
            (requireActivity() as MainActivity).setToolbarTitle(newTitle)
        }
    }

    private fun invalidateWebViewNavigation(){
        val canNavigateBack = viewBinding.webView.canGoBack()
        (requireActivity() as MainActivity).setNavigateUp(canNavigateBack)
    }

    private fun invalidateBookmarkState(){
        viewBinding.webView.url?.let { viewModel.onPageLoaded(it) }
    }

    private fun invalidateLoadingState(progress:Int){
        //we want to have it visible only when the progress is between 2 and 99
        viewBinding.horizontalLoadingBar.isVisible = progress in 2..99
        viewBinding.horizontalLoadingBar.max = 100
        viewBinding.horizontalLoadingBar.progress = progress
    }

    /*No internet connection*/
    private fun onNoInternetConnection(){
        viewBinding.swipeToRefresh.isRefreshing = false
        showDialog(
            title = getString(R.string.no_internet_connection_dialog_title),
            message = getString(R.string.no_internet_connection_dialog_message),
            isCancelable = false
        )
    }

    private fun renderWebViewState(state: NewsWebView.NewsWebViewEvents){
        when(state){
            is NewsWebView.NewsWebViewEvents.TitleReceived -> setToolbarTitle(state.newTitle)
            is NewsWebView.NewsWebViewEvents.ProgressLoading -> {
                invalidateLoadingState(state.progress)
            }
            is NewsWebView.NewsWebViewEvents.PageStart -> {
                viewBinding.swipeToRefresh.isRefreshing = false
                invalidateBookmarkState()
                invalidateWebViewNavigation()
                if(viewModel.showInitialLoading){
                    viewBinding.webView.isVisible = false
                    viewBinding.llLoadingBlockContainer.isVisible = true
                    viewModel.showInitialLoading = false
                    rotateLogo()
                }
            }
            is NewsWebView.NewsWebViewEvents.PageFinished -> {
                viewBinding.swipeToRefresh.isRefreshing = false
                valueAnimator?.cancel()
                viewBinding.llLoadingBlockContainer.isVisible = false
                viewBinding.webView.isVisible = true
                viewBinding.swipeToRefresh.isEnabled = true
            }
            is NewsWebView.NewsWebViewEvents.NoInternetConnection -> {
                onNoInternetConnection()
            }

        }
    }



}