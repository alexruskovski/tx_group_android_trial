package txgroup.trialapp.alexruskovski.presentation.fragment_webview.bookmarks

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import txgroup.trialapp.alexruskovski.data.local.BookmarkEntity
import txgroup.trialapp.alexruskovski.data.local.BookmarksDao
import javax.inject.Inject


/**
 * Created by Alexander Ruskovski on 09/03/2022
 */

@HiltViewModel
class BookmarksFragmentDialogViewModel @Inject constructor(
    private val bookmarksDao: BookmarksDao
): ViewModel() {

    fun getAllBookmarks() = flow {
        emit(bookmarksDao.getAll())
    }

    fun removeBookmark(bookmarkEntity: BookmarkEntity){
        viewModelScope.launch {
            bookmarksDao.delete(bookmarkEntity)
        }
    }

}