package txgroup.trialapp.alexruskovski.presentation.fragment_webview.bookmarks

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.compose.animation.core.animateDpAsState
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import txgroup.trialapp.alexruskovski.R
import txgroup.trialapp.alexruskovski.data.local.BookmarkEntity
import txgroup.trialapp.alexruskovski.databinding.DialogFragmentBinding

/**
 * Created by Alexander Ruskovski on 09/03/2022
 */

@AndroidEntryPoint
class BookmarksFragmentDialog : DialogFragment(R.layout.dialog_fragment){

    private lateinit var binding: DialogFragmentBinding
    private lateinit var viewModel: BookmarksFragmentDialogViewModel

    var onDismiss: () -> Unit = {}
    var onLoadFromBookmark: (String) -> Unit = {}

    private val bookmarksAdapter = lazy {
        BookmarksAdapter()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DialogFragmentBinding.bind(view)
        viewModel = ViewModelProvider(this).get(BookmarksFragmentDialogViewModel::class.java)
        binding.ivClose.setOnClickListener {
            onDismiss()
            dismiss()
        }
        initRecycler()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog?.window?.attributes?.windowAnimations = R.style.AlertDialogFadeInFadeOut
    }

    override fun getTheme(): Int {
        return R.style.FragmentDialogTheme
    }

    private fun initRecycler(){
        binding.rvBookmarks.adapter = bookmarksAdapter.value
        lifecycleScope.launchWhenStarted {
            viewModel.getAllBookmarks().collectLatest { bookmarks ->
                bookmarksAdapter.value.init(
                    data = bookmarks,
                    onDeleteBookmark = { item, position ->
                        bookmarksAdapter.value.notifyItemRemoved(position)
                        onDeleteBookmark(item)
                    },
                    onBookmarkClicked = { item ->
                        onBookmarkClicked(item)
                    }
                )

            }
        }
    }

    private fun onBookmarkClicked(bookmarkEntity: BookmarkEntity){
        onLoadFromBookmark.invoke(bookmarkEntity.url)
        dismiss()
    }

    private fun onDeleteBookmark(bookmarkEntity: BookmarkEntity){
        viewModel.removeBookmark(bookmarkEntity)
    }




}