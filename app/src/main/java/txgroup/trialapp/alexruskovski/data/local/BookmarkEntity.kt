package txgroup.trialapp.alexruskovski.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey


/**
 * Created by Alexander Ruskovski on 08/03/2022
 */
@Entity
data class BookmarkEntity(
    @PrimaryKey(autoGenerate = false)
    val url: String,
    val title: String
) {

    override fun equals(other: Any?): Boolean {
        other ?: return false
        return (other as BookmarkEntity).url.trim() == this.url.trim()
    }

}
