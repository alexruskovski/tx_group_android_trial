package txgroup.trialapp.alexruskovski.data.local

import androidx.room.Database
import androidx.room.RoomDatabase


/**
 * Created by Alexander Ruskovski on 08/03/2022
 */

@Database(
    entities = [
        BookmarkEntity::class
    ],
    version = 1
)
abstract class AppDataBase: RoomDatabase() {
    abstract fun bookmarksDao(): BookmarksDao
}