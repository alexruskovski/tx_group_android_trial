package txgroup.trialapp.alexruskovski.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow


/**
 * Created by Alexander Ruskovski on 08/03/2022
 */

@Dao
interface BookmarksDao {
    @Query("SELECT * FROM bookmarkentity")
    suspend fun getAll(): List<BookmarkEntity>

    @Insert
    suspend fun insert(bookmarkEntity: BookmarkEntity)

    @Delete
    suspend fun delete(bookmarkEntity: BookmarkEntity)

}