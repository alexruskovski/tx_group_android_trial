package txgroup.trialapp.alexruskovski.data.preferences

import android.app.Application
import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Alexander Ruskovski on 08/03/2022
 */

private const val BOOKMARKED_DIALOG = "bookmarkedDialog"

@Singleton
class AppPrefs @Inject constructor(
    application: Application
) {

    private val prefs = application.getSharedPreferences("app_preferences", Context.MODE_PRIVATE)



    fun setIsBookmarkedDialogEnabled(isEnabled: Boolean){
        prefs.edit().putBoolean(BOOKMARKED_DIALOG, isEnabled).apply()
    }
    fun isBookmarkedDialogEnabled() = prefs.getBoolean(BOOKMARKED_DIALOG, true)


}