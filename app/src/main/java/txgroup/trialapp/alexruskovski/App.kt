package txgroup.trialapp.alexruskovski

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import dagger.hilt.android.HiltAndroidApp


/**
 * Created by Alexander Ruskovski on 08/03/2022
 */

@HiltAndroidApp
class App: Application(){

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(
            AppCompatDelegate.MODE_NIGHT_NO)
    }
}