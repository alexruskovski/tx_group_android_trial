package txgroup.trialapp.alexruskovski.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import txgroup.trialapp.alexruskovski.data.local.AppDataBase
import txgroup.trialapp.alexruskovski.data.local.BookmarksDao
import javax.inject.Singleton


/**
 * Created by Alexander Ruskovski on 08/03/2022
 */

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Provides
    @Singleton
    fun providesDatabase(application: Application): AppDataBase{
        return Room.databaseBuilder(
            application,
            AppDataBase::class.java,
            "app_db"
        ).build()
    }

    @Provides
    @Singleton
    fun providesBookmarksDao(appDatabase: AppDataBase): BookmarksDao{
        return appDatabase.bookmarksDao()
    }

}