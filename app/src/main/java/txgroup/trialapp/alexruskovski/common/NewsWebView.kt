package txgroup.trialapp.alexruskovski.common

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.util.Log
import android.webkit.*
import androidx.compose.runtime.mutableStateOf
import kotlinx.coroutines.flow.MutableStateFlow


/**
 * Created by Alexander Ruskovski on 07/03/2022
 */

@SuppressLint("SetJavaScriptEnabled")
class NewsWebView constructor(
    context: Context,
    attributeSet: AttributeSet
): WebView(context, attributeSet) {

    val webViewState = MutableStateFlow<NewsWebViewEvents>(NewsWebViewEvents.PageFinished)

    private val newsWebViewClient = NewsWebViewClient(webViewState)
    private val chromiumClient = object: WebChromeClient(){

        override fun onProgressChanged(view: WebView, newProgress: Int) {
            super.onProgressChanged(view, newProgress)
            webViewState.value = NewsWebViewEvents.ProgressLoading(newProgress)
            Log.d("webview", "onProgressChanged progress $newProgress")
        }

        override fun onReceivedTitle(view: WebView?, title: String?) {
            super.onReceivedTitle(view, title)
            webViewState.value = NewsWebViewEvents.TitleReceived(title ?: "")
            Log.d("webview", "onReceivedTitle title $title")
        }
    }

    init {
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        settings.loadWithOverviewMode = true
        settings.useWideViewPort = true
        settings.builtInZoomControls = true
        settings.displayZoomControls = false
        settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        settings.databaseEnabled = true

        settings.allowFileAccess = true
        settings.allowContentAccess = true

        webChromeClient = chromiumClient
        webViewClient = newsWebViewClient
    }

    fun loadHomePage(){
        loadUrl(Constants.HOME_URL)
        clearHistory()
    }

    fun navigateBackIfPossible(){
        if(canGoBack())
            goBack()
    }

    fun navigateForwardIfCan(){
        if(canGoForward())
            goForward()
    }

    sealed class NewsWebViewEvents {
        object PageFinished: NewsWebViewEvents()
        object PageStart: NewsWebViewEvents()
        object NoInternetConnection: NewsWebViewEvents()
        data class TitleReceived(val newTitle: String): NewsWebViewEvents()
        data class ProgressLoading(val progress:Int): NewsWebViewEvents()
    }

}