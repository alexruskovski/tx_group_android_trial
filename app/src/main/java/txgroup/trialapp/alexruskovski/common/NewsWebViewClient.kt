package txgroup.trialapp.alexruskovski.common

import android.graphics.Bitmap
import android.webkit.*
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * Created by Alexander Ruskovski on 08/03/2022
 *
 * WebViewClient that will be catching the various
 * webview states, like when page is loaded, when it is finished etc.
 */

class NewsWebViewClient constructor(
    private val webViewState: MutableStateFlow<NewsWebView.NewsWebViewEvents>
) : WebViewClient() {

    private var noConnection = false

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
        webViewState.value = NewsWebView.NewsWebViewEvents.PageStart
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        webViewState.value = NewsWebView.NewsWebViewEvents.PageFinished
    }

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        return noConnection
    }

    override fun onReceivedError(
        view: WebView?,
        request: WebResourceRequest?,
        error: WebResourceError?
    ) {
        super.onReceivedError(view, request, error)
        noConnection = error?.errorCode == ERROR_HOST_LOOKUP
        if(error?.errorCode == ERROR_HOST_LOOKUP) {
            webViewState.value = NewsWebView.NewsWebViewEvents.NoInternetConnection
        }
    }

}